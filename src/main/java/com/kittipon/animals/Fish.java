/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.animals;

/**
 *
 * @author kitti
 */
public class Fish extends AquatucAnimal{
    private String nickname;
    public Fish(String nickname) {
        super("FIsh",0);
        this.nickname = nickname;
    }

    @Override
    public void swim() {
        System.out.println("Fish : " + nickname + " swim");
    }

    @Override
    public void eat() {
        System.out.println("Fish : " + nickname + " eat");
    }

    @Override
    public void speak() {
        System.out.println("Fish : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Fish : " + nickname + " sleep");
    }
    
}
