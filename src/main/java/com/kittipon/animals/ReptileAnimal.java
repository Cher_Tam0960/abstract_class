/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.animals;

/**
 *
 * @author kitti
 */
public abstract class ReptileAnimal extends Animal {

    public ReptileAnimal(String name, int numberOfleg) {
        super(name, numberOfleg);
    }

    public abstract void Crawl();
}
