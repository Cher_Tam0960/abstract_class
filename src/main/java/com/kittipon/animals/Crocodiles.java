/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.animals;

/**
 *
 * @author kitti
 */
public class Crocodiles extends ReptileAnimal{
    private String nickname;

    public Crocodiles(String nickname) {
        super("Crocodiles",4);
        this.nickname = nickname;
    }

    @Override
    public void Crawl() {
        System.out.println("Crocodiles : " + nickname + " crawl");
    }

    @Override
    public void eat() {
        System.out.println("Crocodiles : " + nickname + " eat");
    }

    @Override
    public void speak() {
        System.out.println("Crocodiles : " + nickname + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodiles : " + nickname + " sleep");
    }
    
    
}
