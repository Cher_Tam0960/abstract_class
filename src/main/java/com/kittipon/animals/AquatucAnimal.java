/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kittipon.animals;

/**
 *
 * @author kitti
 */
public abstract class AquatucAnimal extends Animal {

    public AquatucAnimal(String name, int numberOfleg) {
        super(name, 0);
    }

    public abstract void swim();
}
